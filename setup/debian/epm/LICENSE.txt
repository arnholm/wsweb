Copyright (C) 2022 Carsten Arnholm arnholm@arnholm.org

wsweb may be used under the terms of either the GNU General 
Public License version 2 or 3 (at your option) as published by the 
Free Software Foundation.

wsweb is provided "AS IS" with NO WARRANTY OF ANY KIND, INCLUDING
THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE.
